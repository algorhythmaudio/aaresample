/*
 *	File:		AAResample.h
 *
 *	Version:	1.0.132
 *
 *	Created:	12-11-24 by Christian Floisand
 *	Updated:	13-01-13 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *	AAResample is a class supporting upsampling/interpolation and downsampling/decimation.  
 *
 *	@usage:--
 *		AAResample::Interpolation interpolator;
 *		interpolator.initialize (srate, factor, quality);
 *			srate is the current sampling rate BEFORE interpolation.
 *			factor (L) is the integer value > 1 to upsample by.
 *			quality is one of resampleQuality_Fast | resampleQuality_Good | resampleQuality_High, 
 *			which affect the speed and strength of the interpolating filter.  
 *			NOTE: the more an audio signal is oversampled, the lower quality of interpolation can be 
 *			afforded.  If a signal is very near Nyquist, or already contains some aliasing, a high-quality
 *			interpolation will be required to keep aliasing from growing or getting worse.
 *
 *		AAResample::Decimation decimator;
 *		decimator.initialize (srate, factor, quality);
 *			srate is the current sampling rate BEFORE decimation.
 *			factor (M) is the integer value > 1 to downsample by.
 *			quality is the same as the argument above for the interpolator, and similar considerations
 *			exist in relation to the incoming audio signal's frequency content.
 *
 *		interpolator.process<float>(input, output, inputSamples);
 *			Interpolates (upsamples) the audio input into the output buffer, which must be a factor of 
 *			L greater than the length of input.
 *			inputSamples is the length of the input buffer.
 *
 *		decimator.process<double>(input, output, outputSamples);
 *			Decimates (downsamples) the audio input into the output buffer, which must be a factor of 
 *			M less than the length of input.
 *			outputSamples is the length of the output buffer.
 *
 *	Additional information:
 *		Any integer can be chosen to use as the resampling factor, and if a value is chosen that is not a 
 *		multiple of the constant defined filter order of the desired quality, AAResample will adjust the 
 *		filter order to accomodate the chosen factor.
 *
 *		As stated above, the sampling rate arguments for both Interpolation and Decimation need to be 
 *		the rate interpolating or decimating FROM. 
 *		i.e. interpolation: srate = 44,100Hz, factor = 3 -> upsampling to 123,300Hz
 *			 decimation: srate = 123,300Hz, factor = 3 -> downsampling back to 44,100Hz
 *
 *		Because the resampling classes use a filter to interpolate/decimate, they include a reset() 
 *		function that must be called after processing is complete to reset the filters' states.
 *		i.e. interpolator.reset();
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#ifndef _AAResample_h_
#define _AAResample_h_

#ifdef AADebug_
#include <iostream>
#include <typeinfo>
#endif

// AALibraries
#include "AADsp.h"
#include "AABasic.h"
#include "AAFIRPolyphase.h"


typedef AADsp::AAFilter::AAFilterBase* AAFilter_ptr;
typedef AADsp::AAFilter::FIRPolyphase<Window::Sinc::LP> InterpFilter;
typedef AADsp::AAFilter::FIRPolyphase<Window::Sinc::LP> DecimFilter;

enum
{
	resampleQuality_Fast,
	resampleQuality_Good,
	resampleQuality_High
};


namespace AADsp {
        
#pragma mark ____AAResample Declaration
////////////////////////////////////////////////////////////////////////
// AAResample namespace
//
// Namespace wrapper containing Interpolation & Decimation classes
// Currently supports only integer-factor upsampling/downsampling
////////////////////////////////////////////////////////////////////////

namespace AAResample {
	
	const float	filterTransitionBWOffset = 620.;
	const ushort filterOrder_Fast = 42;
	const ushort filterOrder_Good = 96;
	const ushort filterOrder_High = 264;
	const ushort maxResampleFactor = 16;
	
	ushort findNearestEvenFilterOrder (ushort fOrder, const ushort factor);
	
	
	// Interpolation (upsampling) class
	//-------------------------------------------------------------------------------------------
	class Interpolation {
	public:
		Interpolation ();
		~Interpolation ();
		
		void initialize (float fromSRate, const ushort interpFactor, const ushort interpQuality);
		void reset		();
		
		// AAResample::Interpolation process
		// inputSamples is length of input buffer
		// output buffer must be a factor of L (interpolation factor) larger than input
		// aAinterpFactor acts as the stride value (L) for the output buffer
		//-------------------------------------------------------------------------------------------
		template <typename Type>
		ushort process (Type *input, Type *output, const ushort inputSamples)
		{
			assert(aAinterpFactor != 1);	// make sure initialize has been called
			assert(input != NULL && output != NULL);
			return ((InterpFilter*)aAinterpFilter)->interpolate<Type>(input, output, aAinterpFactor, inputSamples);
		}
	private:
		AAFilter_ptr aAinterpFilter;
		ushort aAinterpFactor;
	};
	
	
	// Decimation (downsampling) class
	//-------------------------------------------------------------------------------------------
	class Decimation {
	public:
		Decimation ();
		~Decimation ();
		
		void initialize (float fromSRate, const ushort decimFactor, const ushort decimQuality);
		void reset		();
		
		// AAResample::Decimation process
		// outputSamples is length of output buffer
		// input buffer must be a factor of M (decimation factor) larger than input
		// aAdecimFactor acts as the stride value (M) for the input buffer
		//-------------------------------------------------------------------------------------------
		template <typename Type>
		ushort process (Type *input, Type *output, const ushort outputSamples)
		{
			assert(aAdecimFactor != 1);		// make sure initialize has been called
			assert(input != NULL && output != NULL);
			return ((DecimFilter*)aAdecimFilter)->decimate<Type>(input, output, aAdecimFactor, outputSamples);
		}
	private:
		AAFilter_ptr aAdecimFilter;
		ushort aAdecimFactor;
	};
	
	
#if 0
	// Resampling class, for non-integer, rational conversions
	//-------------------------------------------------------------------------------------------
	class Resampling {
	public:
		Resampling ();
		~Resampling ();
		
		void initialize (float fromSRate, float toSRate, const ushort quality);
		void reset		();
		
		// AAResample::Resampling process
		// inputSamples is length of input buffer in fromSRate & outputSamples is length of outputBuffer in toSRate
		// output buffer must be large enough to contain the required number of samples given the conversion rate
		//-------------------------------------------------------------------------------------------
		template <typename Type>
		ushort process (Type *input, Type *output, const ushort inputSamples, const ushort outputSamples)
		{
			assert(aAupFactor != 1 && aAdownFactor != 1);		// make sure initialize has been called
			assert(input != NULL && output != NULL);
			aAtempBuffer = new float[inputSamples*aAupFactor];
			ushort samplesToWrite;
			
			samplesToWrite = ((InterpFilter*)aAupFilter)->interpolate<Type>(input, aAtempBuffer, aAupFactor, inputSamples);
			return ((DecimFilter*)aAdownFilter)->decimate<Type>(aAtempBuffer, output, aAdownFactor, samplesToWrite);
		}
	private:
		AAFilter_ptr aAupFilter, aAdownFilter;
		ushort aAupFactor, aAdownFactor;
		float *aAtempBuffer;
	};
#endif
    
} }	// AAResample namespace // AADsp


#endif	// _AAResample_h_

