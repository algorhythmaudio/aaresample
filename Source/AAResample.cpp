/*
 *	File:		AAResample.cpp
 *
 *	Version:	1.0
 *
 *	Created:	12-11-24 by Christian Floisand
 *	Updated:	12-12-01 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 *  Class definition of resample class.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include "AAResample.h"

using namespace AADsp;


ushort AAResample::findNearestEvenFilterOrder (ushort fOrder, const ushort factor)
{
	short search_factor = -2, last_search;
	while (fOrder % factor != 0 && fOrder > 2) {
		last_search = search_factor;
		fOrder += last_search;
		search_factor = -search_factor;
		last_search = last_search*-1 + search_factor;
	}
	return fOrder;
}

#pragma mark ____Interpolation definition

//
// AAResample::Interpolation constructor/destructor
//-------------------------------------------------------------------------------------------
AAResample::Interpolation::Interpolation () : aAinterpFilter(NULL), aAinterpFactor(1)
{}

AAResample::Interpolation::~Interpolation ()
{
	if (aAinterpFilter)
		delete aAinterpFilter;
}

//
// AAResample::Interpolation initialize
// fromSRate is sample rate before upsampling/interpolation
// factor is integer value, L (>1), for upsampling
//-------------------------------------------------------------------------------------------
void AAResample::Interpolation::initialize (float fromSRate, const ushort interpFactor, const ushort interpQuality)
{
	assert(fromSRate >= MIN_SAMPLE_RATE && fromSRate <= aAsr96000);
	assert(interpFactor > 1 && interpFactor <= maxResampleFactor);
	assert(fromSRate * interpFactor <= MAX_SAMPLE_RATE);
	
	aAinterpFactor = interpFactor;
	ushort filterOrder;
	switch (interpQuality) {
		case resampleQuality_Fast:	filterOrder = filterOrder_Fast;		break;
		case resampleQuality_Good:	filterOrder = filterOrder_Good;		break;
		case resampleQuality_High:	filterOrder = filterOrder_High;		break;
		default:					filterOrder = filterOrder_Fast;
	}
	
	// if factor is not a multiple of the defined filter order of the desired quality, find nearest even filter order
	filterOrder = findNearestEvenFilterOrder(filterOrder, aAinterpFactor);
	double interpFilterCutoff = fromSRate / 2. - filterTransitionBWOffset;	// Nyquist of source sample rate
	
	if (aAinterpFilter)
		delete aAinterpFilter;
	
	// interpolator filter's design needs to set its frequency cutoff in terms of the target sample rate
	AAFilter_ptr filterDesign = new Window::Sinc::LP(interpFilterCutoff, filterOrder, fromSRate*aAinterpFactor);
	aAinterpFilter = new InterpFilter((Window::Sinc::LP*)filterDesign, aAinterpFactor);
	
	// compensate for loss of gain due to interpolation by factor of L = interpolation factor
	aAinterpFilter->setParam(paramID_Gain, ampToDB<double>(static_cast<double>(aAinterpFactor)));
	
	delete filterDesign;
}

//
// AAResample::Interpolation reset
// calls the interpolation filter's reset to clear the filter state
//-------------------------------------------------------------------------------------------
void AAResample::Interpolation::reset ()
{
	aAinterpFilter->reset();
}


#pragma mark ____Decimation definition

//
// AAResample::Decimation constructor/destructor
//-------------------------------------------------------------------------------------------
AAResample::Decimation::Decimation () : aAdecimFilter(NULL), aAdecimFactor(1)
{}

AAResample::Decimation::~Decimation ()
{
	if (aAdecimFilter)
		delete aAdecimFilter;
}

//
// AAResample::Decimation initialize
// fromSRate is sample rate before downsampling/decimation
// factor is integer value, M (>1), for downsampling
//-------------------------------------------------------------------------------------------
void AAResample::Decimation::initialize (float fromSRate, const ushort decimFactor, const ushort decimQuality)
{
	assert(fromSRate >= MIN_SAMPLE_RATE && fromSRate <= MAX_SAMPLE_RATE);
	assert(decimFactor > 1 && decimFactor <= maxResampleFactor);
	assert(fromSRate / decimFactor >= MIN_SAMPLE_RATE);
	
	aAdecimFactor = decimFactor;
	ushort filterOrder;
	switch (decimQuality) {
		case resampleQuality_Fast:	filterOrder = filterOrder_Fast;		break;
		case resampleQuality_Good:	filterOrder = filterOrder_Good;		break;
		case resampleQuality_High:	filterOrder = filterOrder_High;		break;
		default:					filterOrder = filterOrder_Fast;
	}
	
	// if factor is not a multiple of the defined filter order of the desired quality, find nearest even filter order
	filterOrder = findNearestEvenFilterOrder(filterOrder, decimFactor);
	double decimFilterCutoff = fromSRate / (2. * decimFactor) - filterTransitionBWOffset;	// Nyquist of target sample rate
	
	if (aAdecimFilter)
		delete aAdecimFilter;

	// does decimator filter's frequency need to be set in terms of the target sample rate (i.e. currentSr / M)?
	AAFilter_ptr filterDesign = new Window::Sinc::LP(decimFilterCutoff, filterOrder, fromSRate);
	aAdecimFilter = new DecimFilter((Window::Sinc::LP*)filterDesign, aAdecimFactor);
	
	delete filterDesign;
}

//
// AAResample::Decimation reset
// calls the interpolation filter's reset to clear the filter state
//-------------------------------------------------------------------------------------------
void AAResample::Decimation::reset ()
{
	aAdecimFilter->reset();
}


#pragma mark ____Resampling definition
#if 0
//
// AAResample::Resampling constructor/destructor
//-------------------------------------------------------------------------------------------
AAResample::Resampling::Resampling () : aAupFilter(NULL), aAdownFilter(NULL), aAtempBuffer(NULL),
										aAupFactor(1), aAdownFactor(1)
{}

AAResample::Resampling::~Resampling ()
{
	if (aAupFilter)
		delete aAupFilter;
	if (aAdownFilter)
		delete aAdownFilter;
	if (aAtempBuffer)
		delete[] aAtempBuffer;
}

//
// AAResample::Resampling initialize
// fromSRate is sample rate resampling from, and toSRate is the rate resampling to
//-------------------------------------------------------------------------------------------
void AAResample::Resampling::initialize (float fromSRate, float toSRate, const ushort quality)
{
	fromSRate = (fromSRate < MIN_SAMPLE_RATE ? MIN_SAMPLE_RATE : (fromSRate > MAX_SAMPLE_RATE ? MAX_SAMPLE_RATE : fromSRate));
	toSRate = (toSRate < MIN_SAMPLE_RATE ? MIN_SAMPLE_RATE : (toSRate > MAX_SAMPLE_RATE ? MAX_SAMPLE_RATE : toSRate));
	
	ushort commonFactor = static_cast<ushort>( gcd(static_cast<long>(fromSRate), static_cast<long>(toSRate)) );
	aAupFactor = toSRate / commonFactor;
	aAdownFactor = fromSRate / commonFactor;
	
	// set to Nyquist of target sample rate and allow for transition band
	double upFilterCutoff = fromSRate / 2. - filterTransitionBWOffset;
	double downFilterCutoff = (fromSRate * aAupFactor) / (2. * aAdownFactor) - filterTransitionBWOffset;
	ushort filterOrder;
	
	switch (quality) {
		case resampleQuality_Fast:	filterOrder = aAupFactor*2;		break;
		case resampleQuality_Good:	filterOrder = aAupFactor*2;		break;
		case resampleQuality_High:	filterOrder = aAupFactor*3;		break;
		default:					filterOrder = aAupFactor*2;
	}
	
	if (aAupFilter)
		delete aAupFilter;
	if (aAdownFilter)
		delete aAdownFilter;
	
	// design both filters
	AAFilter_ptr filterDesign = new Window::Sinc::LP(upFilterCutoff, filterOrder, fromSRate);
	aAupFilter = new InterpFilter((Window::Sinc::LP*)filterDesign, aAupFactor);
	delete filterDesign;
	
	filterDesign = new Window::Sinc::LP(downFilterCutoff, filterOrder, toSRate);
	aAdownFilter = new DecimFilter((Window::Sinc::LP*)filterDesign, aAdownFactor);
	delete filterDesign;
}

//
// AAResample::Resampling reset
// calls both filter's reset to clear the filter state
//-------------------------------------------------------------------------------------------
void AAResample::Resampling::reset ()
{
	aAupFilter->reset();
	aAdownFilter->reset();
}

#endif

