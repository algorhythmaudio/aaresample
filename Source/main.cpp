//
//  main.cpp
//  AAResample test/demonstration file
//
//  Created by Christian on 2012-11-24.
//  Copyright (c) 2012 Algorhythm Audio. All rights reserved.
//

#include <iostream>
#include <random>
#include <ctime>
#include <cmath>
#include "sndfile.h"
#include "AAOsc.h"
#include "AABasic.h"
#include "AAResample.h"
#include "AAFilterLibrary.h"

#define VECTORSIZE 4096
#define RESAMPLE_FACTOR 2
#define INFILE "/Users/Rainland/Programming/Sounds/triwave.wav"
#define OUTFILE "/Users/Rainland/Desktop/resampled.wav"

int main(int argc, const char * argv[])
{
	using namespace AADsp;
	
    SNDFILE *in, *out;
    SF_INFO in_sfInfo, out_sfInfo;
    clock_t starttime, endtime;
    
    float *inBuffer;
    float *outBuffer;
    float *tempBuffer1;
    float originalSampleRate, newSampleRate;
    
    int readCount, writeCount, samplesToWrite;
    
    if (!(in = sf_open(INFILE, SFM_READ, &in_sfInfo))) {
		printf("Could not open %s\n", INFILE);
		exit(-1);
	}
    
    out_sfInfo = in_sfInfo;
    originalSampleRate = in_sfInfo.samplerate;
    newSampleRate = originalSampleRate * RESAMPLE_FACTOR;
    out_sfInfo.samplerate = newSampleRate;
    
    if (!(out = sf_open(OUTFILE, SFM_WRITE, &out_sfInfo))) {
		printf("Could not open %s\n", OUTFILE);
		exit(-1);
	}
    
    std::cout << "Sample rate of input file: " << in_sfInfo.samplerate << std::endl;
    std::cout << "Resampling by factor of " << RESAMPLE_FACTOR << std::endl;
    std::cout << "Resulting sample rate: " << out_sfInfo.samplerate << std::endl;
    
    inBuffer = new float[VECTORSIZE];
    tempBuffer1 = new float[(int)(VECTORSIZE*RESAMPLE_FACTOR)];
    outBuffer = new float[(int)(VECTORSIZE*RESAMPLE_FACTOR)];
    
    AAOsc lfo(11, originalSampleRate);
	lfo.setType(tOscType_SawUp);
    lfo.setFreq(220);
    lfo.setAmp(1.0);
    
    std::cout << "Processing..." << std::endl;
    
    int i = 0;
    
#define UPSAMPLE 1
#define DOWNSAMPLE 0
#define RINGMOD 0
	
    AAResample::Interpolation interpolator;
    AAResample::Decimation decimator;
    interpolator.initialize(originalSampleRate, RESAMPLE_FACTOR, resampleQuality_Good);
    decimator.initialize(newSampleRate, RESAMPLE_FACTOR, resampleQuality_Good);
    
    starttime = clock();
    
    do  {
        
        readCount = (int)sf_readf_float(in, inBuffer, VECTORSIZE);
        
        if (UPSAMPLE) {
			
            samplesToWrite = interpolator.process<float>(inBuffer, outBuffer, readCount);

        }
        
        if (RINGMOD) {
            for (i = 0; i < (int)(readCount*RESAMPLE_FACTOR); ++i) {
                tempBuffer1[i] = inBuffer[i] * lfo.genInterpLinear();    // use tempBuffer1/2
            }
			lfo.setFreq(lfo.oscFreq()+21);
        }
        
        if (DOWNSAMPLE) {
			
            samplesToWrite = decimator.process<float>(tempBuffer1, outBuffer, readCount);
			
        }
        
        writeCount = (int)sf_writef_float(out, outBuffer, samplesToWrite);
		
		if (writeCount != samplesToWrite)
			std::cout << "Error writing to file. Number of samples processed does not match number of samples written to file.\n";
        
    } while (readCount);
    
    endtime = clock();
	std::cout << "Elapsed time: " << (endtime - starttime) / (double)CLOCKS_PER_SEC << " secs\n";
    std::cout << "Done." << std::endl;
    
    delete[] inBuffer;
    delete[] outBuffer;
    delete[] tempBuffer1;
    sf_close(out);
    
    return 0;
}



